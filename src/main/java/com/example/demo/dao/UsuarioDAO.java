package com.example.demo.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Usuario;

public interface UsuarioDAO extends JpaRepository<Usuario, Long>{

	Optional<Usuario> findByNumeroDocumento(String numeroDocumento);
}
