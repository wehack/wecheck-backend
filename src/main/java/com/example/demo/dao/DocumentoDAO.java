package com.example.demo.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.entity.Documento;

public interface DocumentoDAO extends JpaRepository<Documento, Long>{

	@Query("select d from Documento d where d.usuario.numeroDocumento = ?1")
	List<Documento> buscarDocumentosPorUsuario(String doc);
	
	@Query("select d from Documento d where d.usuario.numeroDocumento = ?1 AND d.codigoDocumento like ?2")
	List<Documento> buscarPorCodigo(String empresa,String codigo);
}
