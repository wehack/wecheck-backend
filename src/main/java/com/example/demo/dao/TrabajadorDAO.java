package com.example.demo.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Trabajador;
import com.example.demo.entity.Usuario;

public interface TrabajadorDAO extends JpaRepository<Trabajador, Long>{

	Optional<Trabajador> findByNumeroDocumento(String numeroDocumento);
}
