package com.example.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class HackatrixApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(HackatrixApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		BCryptPasswordEncoder b = new BCryptPasswordEncoder();
		System.out.println(b.encode("admin"));
		System.out.println(b.encode("12345"));
	}


}
