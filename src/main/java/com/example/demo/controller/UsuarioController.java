package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.common.RequestLogin;
import com.example.demo.service.UsuarioService;

@RestController
@RequestMapping("/auth")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	
	@PostMapping("/nuevo")
	public ResponseEntity<?> nuevoUsuario(@RequestBody RequestLogin request) {
		return usuarioService.nuevoUsuario(request);
	}
}
