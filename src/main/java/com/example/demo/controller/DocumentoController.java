package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Documento;
import com.example.demo.service.DocumentoService;

@RestController
@RequestMapping("/documento")
public class DocumentoController {

	@Autowired
	private DocumentoService documentoService;
	
	@PostMapping("/nuevo")
	public ResponseEntity<?> nuevoDocumento(@RequestBody Documento documento,Authentication authentication) {
		return documentoService.nuevoDocumento(documento,authentication.getName());
	}
	
	@GetMapping("/listar")
	public ResponseEntity<?> listar(Authentication authentication) {
		return documentoService.listar(authentication.getName());
	}
	
	
	@GetMapping("/buscarPorCodigo")
	public ResponseEntity<?> listarPorCodigo(@RequestParam String codigo,Authentication authentication) {
		return documentoService.listarPorCodigo(codigo,authentication.getName());
	}
	
	
}
