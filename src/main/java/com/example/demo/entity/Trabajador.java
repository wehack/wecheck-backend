package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "trabajador",uniqueConstraints = { @UniqueConstraint(columnNames = "numero_documento") })
public class Trabajador {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trabajador_generator")
	@SequenceGenerator(name = "trabajador_generator", sequenceName = "trabajador_sequence", allocationSize = 1)
	private Long id;
	
	private String nombres;
	
	private String apellidos;
	
	@Column(name="numero_documento")
	private String numeroDocumento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	
	
}
