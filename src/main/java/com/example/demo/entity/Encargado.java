package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "encargado")
public class Encargado {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "encargado_generator")
	@SequenceGenerator(name = "encargado_generator", sequenceName = "encargado_sequence", allocationSize = 1)
	private Long id;
	
	private String nombres;
	
	private String apellidos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	
	public String label() {
		return this.getApellidos()+" "+getNombres();
	}
	
}
