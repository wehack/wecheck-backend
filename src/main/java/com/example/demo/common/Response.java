package com.example.demo.common;

public class Response {

	private String mensaje;
	
	private boolean estado;

	public Response() {
		
	}
	
	public Response(String mensaje, boolean estado) {
		super();
		this.mensaje = mensaje;
		this.estado = estado;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
	
}
