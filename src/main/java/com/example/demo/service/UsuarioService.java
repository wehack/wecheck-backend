package com.example.demo.service;

import org.springframework.http.ResponseEntity;

import com.example.demo.common.RequestLogin;

public interface UsuarioService {

	 ResponseEntity<?> nuevoUsuario(RequestLogin request) ;
}
