package com.example.demo.service;

import org.springframework.http.ResponseEntity;

import com.example.demo.entity.Documento;

public interface DocumentoService {

	ResponseEntity<?> nuevoDocumento(Documento request,String empresa) ;
	
	ResponseEntity<?> listar(String empresa) ;
	
	ResponseEntity<?> listarPorCodigo(String codigo,String empresa);
}
