package com.example.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.common.Response;
import com.example.demo.dao.DocumentoDAO;
import com.example.demo.dao.TrabajadorDAO;
import com.example.demo.dao.UsuarioDAO;
import com.example.demo.entity.Documento;
import com.example.demo.entity.Perfil;
import com.example.demo.entity.Trabajador;
import com.example.demo.entity.Usuario;
import com.example.demo.service.DocumentoService;

@Service
public class DocumentoServiceImpl implements DocumentoService{

	@Autowired
	private UsuarioDAO usuarioDAO; 
	
	@Autowired
	private TrabajadorDAO trabajadorDAO;

	@Autowired
	private DocumentoDAO documentoDAO;
	
	@Override
	@Transactional
	public ResponseEntity<?> nuevoDocumento(Documento request, String empresa) {
		try {
			Optional<Usuario> user = usuarioDAO.findByNumeroDocumento(empresa);
			Documento nuevo = new Documento();
			nuevo.setCargo(request.getCargo());
			nuevo.setFechaInicio(request.getFechaInicio());
			nuevo.setFechaFin(request.getFechaFin());
			nuevo.setUsuario(user.get());
			Optional<Trabajador> existente = trabajadorDAO.findById(request.getIdTrabajador()!=null?request.getIdTrabajador():-1);
			if (!existente.isPresent()) {
				Trabajador trabajador = new Trabajador();
				trabajador.setNombres(request.getNombreTrabajador());
				trabajador.setNumeroDocumento(request.getDni());
				trabajadorDAO.save(trabajador);
				nuevo.setTrabajador(trabajador);
			}else {
				nuevo.setTrabajador(existente.get());
			}
			nuevo.setProfesion(request.getProfesion());
			nuevo.setNumeroDocumento(generarNumeroDocumento());
			nuevo.setCodigoDocumento("D00"+nuevo.getNumeroDocumento().toString());
			documentoDAO.save(nuevo);
			return ResponseEntity.ok(new Response("Creado exitosamente.",true));
		} catch (Exception e) {
			return new ResponseEntity<>(new Response(e.getMessage(),false), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<?> listar(String empresa) {
		try {
			List<Documento> lista = documentoDAO.buscarDocumentosPorUsuario(empresa);
			for (Documento documento : lista) {
				System.out.println(documento.getId());
			}
			return ResponseEntity.ok(lista);
		} catch (Exception e) {
			return new ResponseEntity<>(new Response(e.getMessage(), false), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@Override
	public ResponseEntity<?> listarPorCodigo(String codigo,String empresa) {
		try {
			List<Documento> lista = documentoDAO.buscarPorCodigo(empresa,"%"+codigo+"%");
			for (Documento documento : lista) {
				System.out.println(documento.getId());
			}
			return ResponseEntity.ok(lista);
		} catch (Exception e) {
			return new ResponseEntity<>(new Response(e.getMessage(), false), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	public Integer generarNumeroDocumento() {
		List<Documento> docs = documentoDAO.findAll();
		if (docs!=null && !docs.isEmpty()) {
			Integer actual = docs.get(docs.size()-1).getNumeroDocumento();
			actual++;
			return actual;
			//return "D00"+actual.toString();
		}
		return 1;
	}

}
