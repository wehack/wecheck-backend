package com.example.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.common.RequestLogin;
import com.example.demo.common.Response;
import com.example.demo.dao.UsuarioDAO;
import com.example.demo.entity.Perfil;
import com.example.demo.entity.Usuario;
import com.example.demo.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService{

	@Autowired
	private UsuarioDAO usuarioDAO; 
	
	@Autowired 
	private BCryptPasswordEncoder passwordEncoder; 
	
	@Override
	@Transactional
	public ResponseEntity<?> nuevoUsuario(RequestLogin request) {
		try {
			Usuario nuevo = new Usuario();
			nuevo.setNombres(request.getNombres());
			nuevo.setNumeroDocumento(request.getNumeroDocumento());
			nuevo.setPassword(passwordEncoder.encode(request.getPassword()));
			nuevo.setPerfil(request.isEmpresa()?new Perfil((long) 1):new Perfil((long) 2));
			 usuarioDAO.save(nuevo);
			return ResponseEntity.ok(new Response("Creado exitosamente.",true));
		} catch (Exception e) {
			return new ResponseEntity<>(new Response(e.getMessage(),false), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
