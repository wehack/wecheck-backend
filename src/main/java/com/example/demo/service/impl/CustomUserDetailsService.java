package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.dao.UsuarioDAO;
import com.example.demo.entity.Usuario;
@Service
public class CustomUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UsuarioDAO usuarioDAO;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = usuarioDAO.findByNumeroDocumento(username)
				.orElseThrow(() -> new UsernameNotFoundException("User not found with numerodoc: " + username));
		if (usuario==null) {
			//logger.error("No existe el usuario [{}]",username);
			throw new UsernameNotFoundException("No existe en el sistema");
		}
		List<GrantedAuthority> authorities = new ArrayList<>();
		
		authorities.add(new SimpleGrantedAuthority(usuario.getPerfil().getNombre()));
		
		if (authorities.isEmpty()) {
			throw new UsernameNotFoundException("Sin roles");
		}
		return new User(username, usuario.getPassword(), true, true, true, true, authorities);
	}

	
}
