package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.example.demo.security.filter.JWTAuthenticationFilter;
import com.example.demo.security.filter.JWTAuthorizationFilter;
import com.example.demo.security.service.JWTService;
import com.example.demo.service.impl.CustomUserDetailsService;

@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
    private CustomUserDetailsService userDetailService;
   
	@Autowired 
	private BCryptPasswordEncoder passwordEncoder; 
	
	@Autowired 
	private JWTService jWTService; 
	
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
		http
		.cors()
        .and()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
		.authorizeRequests().antMatchers("/","/js/**","/css/**","/auth/**","/error").permitAll()
		.anyRequest().authenticated()
		.and()
		.addFilter(new JWTAuthenticationFilter(authenticationManager(),jWTService))
		.addFilter(new JWTAuthorizationFilter(authenticationManager(),jWTService))
		.csrf().disable()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}
    
	@Autowired
	public void configurerGlobal(AuthenticationManagerBuilder build) throws Exception{
		
		build.userDetailsService(userDetailService)
		.passwordEncoder(passwordEncoder);

	}

}
