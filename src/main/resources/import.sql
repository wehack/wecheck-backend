INSERT INTO perfil(id,nombre) VALUES(nextval('perfil_sequence'),'ROLE_EMPRESA');
INSERT INTO perfil(id,nombre) VALUES(nextval('perfil_sequence'),'ROLE_USUARIO');
INSERT INTO usuario(id,nombres,numero_documento,password,id_perfil) VALUES (nextval('usuario_sequence'),'empresa','12345678901','$2a$10$bUrqMEtCXFqkrbu8bZh9Q.R5zNA/Jub9jCTSwuzwroQtDmwSixlN2',1);
INSERT INTO usuario(id,nombres,numero_documento,password,id_perfil) VALUES (nextval('usuario_sequence'),'usuario1','12345678','$2a$10$3GI/BV.cnRZiL5vw9rCc9.n.o1OgYXWojZ96sGkHZK3INQkVdBFHu',2);
